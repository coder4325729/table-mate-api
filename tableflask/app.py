from flask import Flask, request, jsonify,  render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
from flask_cors import CORS
import mysql.connector
app = Flask(__name__)
CORS(app)

db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="tablemate"
)


mysql = MySQL(app)
# ...

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        cursor = db.cursor()
        cursor.execute("INSERT INTO users (username, password) VALUES (%s, %s)", (username, password))
        db.commit()

        return redirect(url_for('login'))

    return render_template('register.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        cursor = db.cursor()
        username = request.form['username']
        password = request.form['password']
        
        cursor.execute('SELECT * FROM users WHERE username = %s AND password = %s', (username, password))
        user = cursor.fetchone()
        cursor.close()

        if user:
            return redirect(url_for('index'))  # Redirect to the main page after successful login
        else:
            return 'Invalid username/password combination'

    return render_template('login.html')


@app.route('/logout')
def logout():
    return render_template('login.html')

@app.route('/index')
def index():
    
    return render_template('index.html')
    
if __name__ == '__main__':
    app.run(debug=True)
