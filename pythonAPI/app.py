from flask import Flask, request, jsonify, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
from flask_cors import CORS
import mysql.connector

app = Flask(__name__, static_folder='static')
CORS(app)

db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="tablemate"
)

mysql = MySQL(app)


# ...

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        cursor = db.cursor()
        cursor.execute("INSERT INTO users (username, password) VALUES (%s, %s)", (username, password))
        db.commit()

        return redirect(url_for('login'))

    return render_template('register.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        cursor = db.cursor()
        username = request.form['username']
        password = request.form['password']

        cursor.execute('SELECT * FROM users WHERE username = %s AND password = %s', (username, password))
        user = cursor.fetchone()
        cursor.close()

        if user:
            return redirect(url_for('index'))  # Redirect to the main page after successful login
        else:
            return 'Invalid username/password combination'

    return render_template('login.html')


@app.route('/logout')
def logout():
    return render_template('login.html')


@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/reservations', methods=['GET', 'POST'])
def reservations():
    if request.method == 'POST':
        name = request.form['Name']
        email = request.form['Email']
        contact = request.form['Contact']
        persons = request.form['Persons']
        day = request.form['Day']
        time = request.form['Time']
        note = request.form['Note']
        occassion = request.form['Occassion']

        cursor = db.cursor()
        cursor.execute('SELECT * FROM reservations ORDER BY id DESC')  # Replace with your specific query if needed
        reservations_data = cursor.fetchall()
        cursor.close()
        print(reservations_data)

        return render_template('reservations.html', reservations=reservations_data)

    # Handle the initial GET request here
    cursor = db.cursor()
    cursor.execute('SELECT * FROM reservations ORDER BY id DESC')
    reservations_data = cursor.fetchall()
    cursor.close()
    print(reservations_data)

    return render_template('reservations.html', reservations=reservations_data)




@app.route('/reserve', methods=['GET', 'POST'])
def reserve():
    if request.method == 'POST':
        name = request.form['Name']
        email = request.form['Email']
        contact = request.form['Contact']
        persons = request.form['Persons']
        day = request.form['Day']
        time = request.form['Time']
        note = request.form['Note']
        occassion = request.form['Occassion']

        cursor = db.cursor()
        cursor.execute("INSERT INTO reservations (name, email, contact, persons, date, time, note, occassion) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       (name, email, contact, persons, day, time, note, occassion))
        db.commit()

        return redirect(url_for('reserve'))

    return render_template('addreservations.html')


@app.route('/delete_reservation/<int:reservation_id>', methods=['DELETE'])
def delete_reservation(reservation_id):
    cursor = db.cursor()
    cursor.execute('DELETE FROM reservations WHERE id = %s', (reservation_id,))
    db.commit()
    cursor.close()
    return 'Reservation deleted', 204


@app.route('/update_status/<int:reservation_id>/<new_status>', methods=['PUT'])
def update_status(reservation_id, new_status):
    cursor = db.cursor()
    cursor.execute('UPDATE reservations SET status = %s WHERE id = %s', (new_status, reservation_id))
    db.commit()
    cursor.close()
    return 'Status updated', 204


if __name__ == '__main__':
    app.run(debug=True)
