<?php
include("../db/config.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $confirm_password = $_POST["confirm_password"];

    // Check if the username already exists
    $check_stmt = $conn->prepare("SELECT id FROM users WHERE username = ?");
    $check_stmt->bind_param("s", $username);
    $check_stmt->execute();
    $check_stmt->store_result();

    if ($check_stmt->num_rows > 0) {
        echo '<script>window.location.href = "../register.php"; alert("Username already exist!"); </script>';
    } else {
        // Check if password and confirm password match
        if ($password !== $confirm_password) {
            echo '<script>window.location.href = "../register.php"; alert("Password do not match!"); </script>';
        } else {
            // Prepare and execute the SQL query to insert user data into the database
            $stmt = $conn->prepare("INSERT INTO users (username, first_name, last_name, email, password) VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", $username, $first_name, $last_name, $email, $password);

            if ($stmt->execute()) {
                echo '<script>window.location.href = "../register.php"; alert("Registration Successful!"); </script>';
            } else {
                echo '<script>window.location.href = "../register.php"; alert("Error signing in!"); </script>';
            }

            $stmt->close();
        }
    }

    $check_stmt->close();
    $conn->close();
}
?>
