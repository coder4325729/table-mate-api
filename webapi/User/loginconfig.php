<?php
include("../db/config.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    // Prepare and execute the SQL query
    $stmt = $conn->prepare("SELECT id, password FROM users WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->bind_result($id, $dbPassword);

    // Check if a row was fetched
    if ($stmt->fetch()) {
        // Compare the plaintext password
        if ($password === $dbPassword) {
            // Login successful, redirect to main page
            echo '<script>window.location.href = "../index2.html"; alert("Login successful!"); </script>';
            exit();
        } else {
            // Incorrect password
            echo '<script>alert("Invalid Account!"); window.location.href = "../login.php";</script>';
        }
    } else {
        // User not found
        echo '<script>alert("User not found!"); window.location.href = "../login.php";</script>';
    }

    // Close the statement and database connection
    $stmt->close();
    $conn->close();
}
?>
