<?php
require 'connection.php';

// Create a query to retrieve menu items from your database
$query = "SELECT * FROM menu";
$result = mysqli_query($conn, $query);

$menuItems = array();

if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
        $menuItems[] = $row;
    }
}

// Return the menu items as JSON
header('Content-Type: application/json');
echo json_encode($menuItems);
?>