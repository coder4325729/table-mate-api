<?php
session_start();
require_once('connection.php');

$id = $_GET["id"];
$RestoName = $_POST['RestoName'];
$RestoAddress = $_POST['RestoAddress'];
$Number = $_POST['Number'];
$BusHrs = $_POST['BusHrs'];
$Number = $_POST['Number'];

if (isset($_GET['username'])) {
    $id = $_GET['username'];
    // Create a prepared statement
    $stmt = $conn->prepare("SELECT * FROM users WHERE username = ?");
    $stmt->bind_param("i", $id); // 'i' represents an integer (assuming MenuID is an integer).

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $menu = $result->fetch_assoc();
            // Now you can use the $menu data.
        } else {
            echo "No records found.";
        }
        $stmt->close();
    } else {
        echo "Query execution failed.";
    }
} else {
    echo "ID not provided in the URL.";
}

if (isset($_POST['save'])) {
    if (!empty($_FILES["file"]["name"])) {
    
        $fileName = $_FILES["file"]["name"];
        $fileType = $_FILES["file"]["type"];
        $fileContent = file_get_contents($_FILES["file"]["tmp_name"]);

        $newFileName = uniqid() . "-" . $fileName;

        // Check if the uploaded file is an image (you can extend this to include more image types)
        $allowedImageTypes = ["image/jpeg", "image/png", "image/gif"];
        if (in_array($fileType, $allowedImageTypes)) {
            // Use a prepared statement to insert the data
            $stmt = $conn->prepare("UPDATE users SET RestoName = ?, RestoAddress = ?, Number = ?, BusHrs = ?, Logo = ? WHERE id = ?");
            $stmt->bind_param("sissi", $RestoName, $RestoAddress, $Number, $BusHrs, $fileContent, $id);

            if ($stmt->execute()) {
                $_SESSION['status'] = "User Info Edited Successfully";
                header('location: index.php');
            } else {
                $_SESSION['status'] = "Error: " . $stmt->error;
                header('location: index.php');
            }
        } else {
            $_SESSION['status'] = "Sorry, only JPG, PNG, and GIF images are allowed";
            header('location: index.php');
        }
    } else {
        $_SESSION['status'] = "Please attach an image";
        header('location: index.php');
    }
    $conn->close();
}
?>