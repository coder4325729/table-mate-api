<?php
session_start();
require_once('connection.php');

if (isset($_POST['save'])) {
    if (!empty($_FILES["file"]["name"])) {
        $ItemName = $_POST['ItemName'];
        $Price = $_POST['Price'];
        $Persons = $_POST['Persons'];
        

        $fileName = $_FILES["file"]["name"];
        $fileType = $_FILES["file"]["type"];
        $fileContent = file_get_contents($_FILES["file"]["tmp_name"]);

        // Check if the uploaded file is an image (you can extend this to include more image types)
        $allowedImageTypes = ["image/jpeg", "image/png", "image/gif"];
        if (in_array($fileType, $allowedImageTypes)) {
            // Use a prepared statement to insert the data
            $stmt = $conn->prepare("INSERT INTO menu (ItemName, Price, Persons, Photo) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("siss", $ItemName, $Price, $Persons, $fileContent);

            if ($stmt->execute()) {
                $_SESSION['status'] = "New Menu Added Successfully";
                header('location: menu.php');
            } else {
                $_SESSION['status'] = "Error: " . $stmt->error;
                header('location: menu.php');
            }
        } else {
            $_SESSION['status'] = "Sorry, only JPG, PNG, and GIF images are allowed";
            header('location: menu.php');
        }
    } else {
        $_SESSION['status'] = "Please attach an image";
        header('location: menu.php');
    }
    $conn->close();
}
?>