<?php
session_start();
require 'connection.php';

if (isset($_POST['MenuID'])) {
    $MenuID = $_POST['MenuID'];
    $query = "DELETE FROM menu WHERE MenuID = '$MenuID'";
    if (mysqli_query($conn, $query)) {
        $_SESSION['status'] = "Item Menu Deleted Successfully";
        header('location: menu.php');
    } else {
        $_SESSION['status'] = "Error: " . mysqli_error($conn);
        header('location: menu.php');
    }
} else {
    $_SESSION['status'] = "MenuID not provided in the POST request.";
    header('location: menu.php');
}

?>