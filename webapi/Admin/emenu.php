<?php
session_start();
require_once('connection.php');

$id = $_GET["MenuID"];
$ItemName = $_POST['ItemName'];
$Price = $_POST['Price'];
$Persons = $_POST['Persons'];

if (isset($_GET['MenuID'])) {
    $id = $_GET['MenuID'];
    // Create a prepared statement
    $stmt = $conn->prepare("SELECT * FROM menu WHERE MenuID = ?");
    $stmt->bind_param("i", $id); // 'i' represents an integer (assuming MenuID is an integer).

    if ($stmt->execute()) {
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $menu = $result->fetch_assoc();
            // Now you can use the $menu data.
        } else {
            echo "No records found.";
        }
        $stmt->close();
    } else {
        echo "Query execution failed.";
    }
} else {
    echo "MenuID not provided in the URL.";
}

if (isset($_POST['save'])) {
    if (!empty($_FILES["file"]["name"])) {
    
        $fileName = $_FILES["file"]["name"];
        $fileType = $_FILES["file"]["type"];
        $fileContent = file_get_contents($_FILES["file"]["tmp_name"]);

        $newFileName = uniqid() . "-" . $fileName;

        // Check if the uploaded file is an image (you can extend this to include more image types)
        $allowedImageTypes = ["image/jpeg", "image/png", "image/gif"];
        if (in_array($fileType, $allowedImageTypes)) {
            // Use a prepared statement to insert the data
            $stmt = $conn->prepare("UPDATE menu SET ItemName = ?, Price = ?, Persons = ?, Photo = ? WHERE MenuID = ?");
            $stmt->bind_param("sissi", $ItemName, $Price, $Persons, $fileContent, $id);

            if ($stmt->execute()) {
                $_SESSION['status'] = "Item Menu Edited Successfully";
                header('location: menu.php');
            } else {
                $_SESSION['status'] = "Error: " . $stmt->error;
                header('location: menu.php');
            }
        } else {
            $_SESSION['status'] = "Sorry, only JPG, PNG, and GIF images are allowed";
            header('location: menu.php');
        }
    } else {
        $_SESSION['status'] = "Please attach an image";
        header('location: menu.php');
    }
    $conn->close();
}
?>