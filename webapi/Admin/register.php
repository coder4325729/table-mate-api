<?php
include("connection.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $restoname = $_POST["restoname"];
    $restoaddress = $_POST["restoaddress"];
    $username = $_POST["username"];
    $email = $_POST["email"];
    $password = $_POST["password"];

    // Check if the username already exists
    $check_stmt = $conn->prepare("SELECT id FROM users WHERE username = ?");
    $check_stmt->bind_param("s", $username);
    $check_stmt->execute();
    $check_stmt->store_result();

    if ($check_stmt->num_rows > 0) {
        echo '<script>window.location.href = "logreg.php"; alert("Username already exist!"); </script>';
    } else {
        // Check if password and confirm password match
        
            // Prepare and execute the SQL query to insert user data into the database
            $stmt = $conn->prepare("INSERT INTO users (RestoName, RestoAddress, username, email, password) VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", $restoname, $restoaddress, $username, $email, $password);

            if ($stmt->execute()) {
                echo '<script>window.location.href = "index.php"; alert("Registration Successful!"); </script>';
            } else {
                echo '<script>window.location.href = "logreg.php"; alert("Error signing in!"); </script>';
            }

            $stmt->close();
        
    }

    $check_stmt->close();
    $conn->close();
}
?>
