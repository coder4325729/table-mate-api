<?php
require_once('db_config.php');

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    $sql = "SELECT * FROM users WHERE username='$username'";
    $result = mysqli_query($db, $sql);

    if ($result) {
        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row['password'])) {
            $response['success'] = true;
            $response['message'] = "Login successful!";
            $response['user'] = array(
                'id' => $row['id'],
                'username' => $row['username'],
                'email' => $row['email']
            );
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid username or password.";
        }
    } else {
        $response['success'] = false;
        $response['message'] = "Invalid username or password.";
    }
} else {
    $response['success'] = false;
    $response['message'] = "Invalid request method";
}

echo json_encode($response);
?>