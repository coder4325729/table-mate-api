
<?php
require_once('db_config.php');

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $Name = mysqli_real_escape_string($db, $_POST['Name']);
    $Email = mysqli_real_escape_string($db, $_POST['Email']);
    $Persons = mysqli_real_escape_string($db, $_POST['Persons']);
    $Contact = mysqli_real_escape_string($db, $_POST['Contact']);
    $Note = mysqli_real_escape_string($db, $_POST['Note']);
    $Occassion = mysqli_real_escape_string($db, $_POST['Occassion']);
    $Day = mysqli_real_escape_string($db, $_POST['Day']);
    $Time = mysqli_real_escape_string($db, $_POST['Time']);
    // Hash the password for security
   

    $sql = "INSERT INTO bookings (Name, Email, Contact, Persons, Day, Time, Note, Occassion) VALUES ('$Name', '$Email', '$Contact', '$Persons', '$Day', '$Time', '$Note', '$Occassion')";

    if (mysqli_query($db, $sql)) {
        $response['success'] = true;
        $response['message'] = "Reservation successful!";
    } else {
        $response['success'] = false;
        $response['message'] = "Reservation failed. Please try again.";
    }
} else {
    $response['success'] = false;
    $response['message'] = "Invalid request method";
}

echo json_encode($response);
?>
