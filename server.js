const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const bcrypt = require('bcrypt'); // Import bcrypt for password hashing

const app = express();
const port = 3000;

app.use(bodyParser.json());

const db = mysql.createConnection({
  host: 'localhost',
  user: 'your_db_username',
  password: 'your_db_password',
  database: 'userdata'
});

db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log('Connected to database');
});

// Step 4: Create Signup Endpoint
app.post('/signup', (req, res) => {
  const { username, email, password } = req.body;
  const hashedPassword = bcrypt.hashSync(password, 10); // Use bcrypt to hash the password

  const sql = 'INSERT INTO users (username, email, password) VALUES (?, ?, ?)';
  db.query(sql, [username, email, hashedPassword], (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).json({ error: 'Error signing up' });
    } else {
      res.json({ success: true, message: 'Registration successful!' });
    }
  });
});

// Step 3: Start the Server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
